package com.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisUtil {

    public static Jedis getJedis(){
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(200);
        config.setMaxIdle(2);
        config.setMaxWaitMillis(100*1000);
        JedisPool pool = new JedisPool(config,"127.0.0.1",6379,3000);
        return pool.getResource();
    }

    public static JedisPool getJedisPool(){
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(200);
        config.setMaxIdle(2);
        config.setMaxWaitMillis(100*1000);
        return new JedisPool(config,"47.103.98.92",6379,3000);
    }
}
