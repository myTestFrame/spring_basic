package com.redis;

import redis.clients.jedis.Jedis;

/**
 * zset结构
 * 可能是redis 提供的最有特色的机构，类似于 SordedSet 和 HashMap的合体
 * 一方面是set,保证唯一，另外，每个value 有个 score,代表value的排序权重，这个score可以重复
 * 通过跳跃列表来实现
 * 常见场景：微博热点话题，粉丝列表
 */
public class ZsetRedis {
    public static void main(String[] args) {
        Jedis jedis = JedisUtil.getJedis();
        // 添加元素
        jedis.del("name");
        jedis.zadd("name",10,"xudong1");
        jedis.zadd("name",9,"xudong2");
        jedis.zadd("name",8,"xudong3");
        // 按权重顺序排
        System.out.println(jedis.zrange("name",0,-1));
        // 逆序排
        System.out.println(jedis.zrevrange("name",0,-1));


    }
}
