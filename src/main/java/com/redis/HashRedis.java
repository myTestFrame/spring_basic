package com.redis;

import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.Map;

/**
 * Hash结构：
 * 相当于 HashMap,field ->value 键值对，放到一个key 中，每个key 放多个键值对
 * 不同的是： 1 value 只能存储字符串 2 rehash 方法不同
 * rehash: 不是一下子全部hash，而是渐进式的，会保留原有的hash ,慢慢的hash,查询会同时查询，直到完成，删除老的
 * 缺点： 效率没有string 高
 * 关于内部实现，以后讨论
 */
public class HashRedis {
    public static void main(String[] args) {
        Jedis jedis=JedisUtil.getJedis();
        jedis.del("name");
        // 添加单个键值对
        jedis.hset("name","tom"," he is boy");
        jedis.hset("name","xudong","he is boy too");
        System.out.println(jedis.hget("name","tom"));
        System.out.println(jedis.hget("name","xudong"));
        Map map = new HashMap();
        // 添加整个map
        map.put("name1","xudong1");
        map.put("name2","xudong2");
        jedis.hmset("name",map);
        // 获取hash 所有键值对
        Map all = jedis.hgetAll("name");
        System.out.println(all);
        // 获取元素个数
        Long count = jedis.hlen("name");
        System.out.println(count);

    }
}
