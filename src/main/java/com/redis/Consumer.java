package com.redis;

import redis.clients.jedis.Jedis;

import java.util.List;

/**
 * 消息队列消费者,
 * brpop：阻滞读，不会一直读空的
 * 但是闲置阻滞太久，会报错的，如果捕获异常，还要重试
 */
public class Consumer {
    public static void main(String[] args) throws InterruptedException {
        Jedis jedis = JedisUtil.getJedis();
        while(true){
            Thread.sleep(1000);
            List<String> list = jedis.brpop("mq","0");
            System.out.println(list);
       }
    }
}
