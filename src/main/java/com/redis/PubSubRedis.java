package com.redis;

import redis.clients.jedis.Jedis;

/**
 * 消息队列，支持多播的模式
 * PubSub消息多播模块
 * 不再使用那5种基本的数据结构，用发布订阅模式
 * 生产者只生产一次消息，然后中间件将消息放到各个消息队列，让消费者消费，它是分布式系统的常用解耦方式
 */
public class PubSubRedis {
    public static void main(String[] args) {
        Jedis jedis = JedisUtil.getJedis();
        // 生产两条消息
        jedis.publish("mq","mq1");
        jedis.publish("mq","mq2");
    }
}
