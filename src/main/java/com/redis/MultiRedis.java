package com.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

/**
 *  redis 事务，没有回滚
 *  multi: 开启
 *  exec ： 执行,这之前的指令，先缓存到事务队列中，然后一起执行
 *  discard： 放弃当前事务，丢弃缓存队列中的所有指令
 *  不具备原子性，只保证了隔离性
 */
public class MultiRedis {
    public static void main(String[] args) {
        Jedis jedis = JedisUtil.getJedis();
        Transaction tc = jedis.multi();
        tc.discard();
        tc.exec();

    }
}
