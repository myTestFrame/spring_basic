package com.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.Set;

public class RedisMq {
    public RedisMq() {
    }

    public static void main(String[] args) {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(200);
        config.setMaxIdle(2);
        config.setMaxWaitMillis(100*1000);
        JedisPool pool = new JedisPool(config,"127.0.0.1",6379,3000);
        Jedis jedis = pool.getResource();
        // 共5种数据结构 String  list set zset hash
        // 延时队列使用 zset,它类似于 SortedSet 和 HashMap 的合体, key --> value ,value 还有一个 score 权重
        jedis.zadd("name",10,"张三");
        jedis.zadd("name",9,"李四");
        Set<String> set = jedis.zrange("name",0,-1);
        System.out.println(set);


    }
}
