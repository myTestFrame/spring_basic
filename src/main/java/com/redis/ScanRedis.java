package com.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;

/**
 * 大海捞针Scan
 * 从海量的 key中找出符合条件的
 * redis的keys方法: 通过匹配，但没有 limit offset 限制，遍历所有的的，复杂度是 O(N),最重要会阻滞线程
 * scan:
 * 1 提供limit参数，控制返回结果的最大条数
 * 2 复杂度 O(N),通过游标分离，不会阻滞线程
 */
public class ScanRedis {
    public static void main(String[] args) {
        Jedis jedis = JedisUtil.getJedis();
        jedis.del("name");
        for (int i = 0; i < 1000; i++) {
            jedis.set("name"+i,"xudong"+i);
        }
        System.out.println(jedis.keys("name*"));

        /**
         * scan 提供了3个参数
         * cursor:第一次为 0
         * key正则：
         * limit hint:
         */
        for (int i = 0; i < 10000; i++) {
            jedis.set("name"+i,"xudong"+i);
        }
        ScanParams params = new ScanParams();
        params.match("name99*");
        params.count(1); // 限制 的不是返回数量，而是遍历的槽位数
        ScanResult<String> res = jedis.scan("0",params);

        System.out.println(res.getCursor()+"\n"+res.getResult());
    }
}
