package com.redis;

import redis.clients.jedis.Jedis;

/**
 * 问题：统计网页点击量，每个用户多次点击算一次
 * 方案一：redis计数器，每点击一次，增加一个数，不行，不能区别用户，有重复点击的
 * 方案二：redis 的set 结构，如果有上亿的点击，集合太大了
 * 实际上，这个数字不用精确，100万和101 万差别可以忽略了，所以我们引入 HyperLogLog 数据结构
 * 终极方法：HyperLogLog高级数据结构,误差率 0.81%,类似于 set 结构
 * 注意这个不是免费的，需要占用 12Kb的存储空间，不适合统计单个用户的数据，如果用户很多，占据空间很大
 * 在元素较少时，采用稀疏矩阵，多时，才分配12kb，采用稠密矩阵
 * 原理比较复杂：
 *
 */
public class HyperLogLogRedis {
    public static void main(String[] args) {
        Jedis jedis = JedisUtil.getJedis();
        jedis.del("key");
        for(int i=0;i<100000;i++){
            // 增加
            jedis.pfadd("key","xudong"+i) ;
        }
        // 统计总数
        System.out.println(jedis.pfcount("key"));
        // 可以发现，数额较大时，有误差 测试 是 100173，实际我们添加了100000

        // 除了 pfadd 和pfcount ,还有psmerge,将两个计数合并
        jedis.del("key0");
        for(int i=0;i<100000;i++){
            // 增加
            jedis.pfadd("key0","xu"+i) ;
        }
        System.out.println(jedis.pfcount("key0"));
        // 将 key0 的元素合并到 key,可以看到，也有一定误差
        System.out.println(jedis.pfmerge("key","key0"));
        System.out.println(jedis.pfcount("key"));
    }
}
