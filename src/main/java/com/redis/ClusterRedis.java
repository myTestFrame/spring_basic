package com.redis;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashSet;
import java.util.Set;

public class ClusterRedis {
    public static void main(String[] args) {
        HostAndPort hs1 = new HostAndPort("192.168.0.110",9001);
        HostAndPort hs2 = new HostAndPort("192.168.0.110",9001);
        HostAndPort hs3 = new HostAndPort("192.168.0.110",9001);
        HostAndPort hs4 = new HostAndPort("192.168.0.110",9001);
        HostAndPort hs5 = new HostAndPort("192.168.0.110",9001);
        HostAndPort hs6 = new HostAndPort("192.168.0.110",9001);
        Set<HostAndPort> set = new HashSet<>();
        set.add(hs1);
        set.add(hs2);
        set.add(hs3);
        set.add(hs4);
        set.add(hs5);
        set.add(hs6);
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(200);
        config.setMaxIdle(2);
        config.setMaxWaitMillis(100*1000);
        JedisCluster jc = new JedisCluster(set,1000,1000,200,"123456",config);
        jc.set("name","xudong");
        jc.close();
    }
}
