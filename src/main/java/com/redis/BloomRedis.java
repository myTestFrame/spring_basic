package com.redis;

import io.rebloom.client.Client;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import redis.clients.jedis.Jedis;

/**
 * 注意：本例切换到 阿里云的redis,本地没有安装插件
 * 布隆过滤器,redis 4.0 提供插件功能
 * HyperLogLog 数据结构进行估计，能解决精确度不高的场景，但我们要判断给定元素是否存在，就不行了
 * 场景；浏览新闻自动推荐，过滤掉已经看过的内容,爬虫系统，过滤已经爬取的网页，垃圾邮件功能
 * 方案一：关系数据库这样的场景，需要用exists 判断，效率很低
 * 方案二：使用缓存，但将如此多的历史记录缓存，时间久了，也不行
 * 本例布隆过滤器 能解决去重问题，过滤看过的内容，节省 90% 空间，只是有点不精确，可能会过滤掉部分没看的内容
 * 即 有的没有推荐，但是能保证看过的绝对不会推荐。
 * jedis 3.0 以前没有提供命令扩展,3.0 是 新的版本
 */
public class BloomRedis {
    public static void main(String[] args) {
        // 使用 RedisLabs 的包下的 JReBloom 包的Client ,单独操作
        Client client = new Client(JedisUtil.getJedisPool());
        // 创建一个有初始值和出错率的过滤器
       // client.createFilter("bloomKey",100000,0.01);
        client.delete("bloomKey");
        for (int i = 0; i < 10000; i++) {
            client.add("bloomKey","user"+i);
            boolean res = client.exists("bloomKey","user"+(i+1));
            if(!res){
                System.out.println(i);
                break;
            }
        }
    }
}
