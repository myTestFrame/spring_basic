package com.redis;

import redis.clients.jedis.BinaryJedisPubSub;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

import java.util.List;

/**
 * 多播消费者
 */
public class PubSubConsumer {
    public static void main(String[] args) {
        Jedis jedis = JedisUtil.getJedis();
        BinaryJedisPubSub jedisPubSub = new BinaryJedisPubSub() {
            @Override
            public void onMessage(byte[] channel, byte[] message) {
                super.onMessage(channel, message);
            }
        };
        List<String> list = jedis.pubsubChannels("mq");
        System.out.println(list);
    }
}
