package com.redis;

import redis.clients.jedis.GeoCoordinate;
import redis.clients.jedis.GeoRadiusResponse;
import redis.clients.jedis.GeoUnit;
import redis.clients.jedis.Jedis;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * GeoHash:地理元素模块
 * 实现附近的膜拜单车，附近人功能,业内常用的地理位置排除算法
 * 二刀切法：将二维的变成一维整数，整数越长，越精确，然后对整数 base32 编码
 * Redis 提供的Geo指令只有 6 个，是一个普通的 zset 结构
 *  geoadd ;增加
 *  zrem:删除
 *  geodist:计算距离
 *  geopops:获取元素的坐标
 *  geohash: 直接获取 base32编码值，可以去 网上直接定位：http://geohash.org/${base32}
 *  georadiusbymember: 附近的人，最重要的功能
 *  需要注意，最好采用单个服务器，不使用集群，涉及到节点迁移，如果单个key 过大，会对迁移造成影响
 */
public class GeoHashRedis {
    public static void main(String[] args) {
        Jedis jedis = JedisUtil.getJedis();
        jedis.del("name");
        // 添加 集合名为 name, 附带经纬度
        jedis.geoadd("name",116.00,39.00,"xudong");
        jedis.geoadd("name",110,40,"xudong1");
        GeoCoordinate coordinate1 = new GeoCoordinate(123,33.999);
        GeoCoordinate coordinate2 = new GeoCoordinate(114.89,44.20);
        Map<String,GeoCoordinate> map = new HashMap<>();
        map.put("xudong3",coordinate1);
        map.put("xudong4",coordinate2);
        // 添加多个
        jedis.geoadd("name",map);

        // 计算距离 xudong 和 xudong1 之间的距离,默认单位是米 ，GeoUnit可去掉
        System.out.println(jedis.geodist("name","xudong","xudong1", GeoUnit.KM));

        // 获取任意元素的坐标，可以看到，有偏差，因为对二维左边进行一维映射有损失，还原回来造成偏差
        System.out.println(jedis.geopos("name","xudong","xudong3"));

        // 获取 base32　编码,网上直接定位：http://geohash.org/${base32}
        System.out.println(jedis.geohash("name","xudong2","xudong4"));

        // 指定元素附近的其他元素, xudong  600 km 范围,不会排除自身
        List<GeoRadiusResponse> list = jedis.georadiusByMember("name","xudong",600,GeoUnit.KM);
        for (GeoRadiusResponse member:list) {
            System.out.println(member.getMemberByString());
        }

    }
}
