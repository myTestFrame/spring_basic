package com.redis;

import redis.clients.jedis.Jedis;

/**
 * List 结构：
 * 相当于 LinkedList,链表，意味着插入和删除快速，查询慢，双向链表
 * 可以作为队列，也可以作为栈结构，即可以先进先出，也可以后进先出
 * 适合异步队列使用
 * 底层其实 是 快速列表 : zipList 连续的内存 + 链表 ，多个 ziplist压缩链表用双向指针连接起来
 */
public class ListRedis {
    public static void main(String[] args) {
        Jedis jedis = JedisUtil.getJedis();
        jedis.del("name");
        jedis.lpush("name","xudong1","xudong2"); // 链表左边插入
        jedis.rpush("name","xudong3","xudong4"); // 链表右边插入
        System.out.println(jedis.lrange("name",0,-1)); // 从左到右获取所有元素
        System.out.println(jedis.lindex("name",0));//  lindex 查询某个下标的元素，不会弹出
        System.out.println(jedis.lpop("name")); // 左边弹出一个元素
        System.out.println(jedis.rpop("name")); // 右边弹出一个元素
        System.out.println(jedis.llen("name")); // 链表长度

    }
}
