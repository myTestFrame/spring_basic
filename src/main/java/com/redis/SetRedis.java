package com.redis;

import redis.clients.jedis.Jedis;

/**
 * Set结构：
 * 相当于HashSet,唯一的、无序的，可以存放要求去重复的数据，比如中奖号码
 * 此外，set 集合在服务器端完成的 union  等集合的聚合交集并集操作，效率很高，节省了网络IO 开销
 */
public class SetRedis {
    public static void main(String[] args) {
        Jedis jedis = JedisUtil.getJedis();
        jedis.del("name");
        // 添加操作，可以多个,成功就返回添加的个数，如果有了，就不会添加
        jedis.sadd("name","xudong1","xudong2","xudong3");
        //查看集合成员，无序的
        System.out.println(jedis.smembers("name"));
        // 查看是否包含某个元素,相当于contains
        System.out.println(jedis.sismember("name","xudong1"));
        // 获取元素个数
        System.out.println(jedis.scard("name"));
        // 随机弹出一个元素
        System.out.println(jedis.spop("name"));
        // 删除某个值得元素，可以一起多个
        jedis.srem("name","xudong1","xudong2");
        System.out.println(jedis.scard("name"));

        //下面高级应用，元素的聚合
        jedis.del("key1","key2");
        jedis.sadd("key1","java","pathon","C","Go");
        jedis.sadd("key2","java","数学");
        // 将集合 key2 中的元素 移动到 key1
        jedis.smove("key2","key1","数学");
        System.out.println(jedis.smembers("key1"));
        System.out.println(jedis.smembers("key2"));

        // 求交集,不会改变元集合
        System.out.println(jedis.sinter("key1","key2"));
        // 求并集，不改变元集合
        System.out.println(jedis.sunion("key1","key2"));
        // 求差集
        System.out.println(jedis.sdiff("key1","key2"));

    }
}
