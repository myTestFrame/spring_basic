package com.redis;

import redis.clients.jedis.Jedis;

import java.util.List;

/**
 *  String结构：
 * 最简单的数据结构，常见的场景就是缓存用户信息，我们将信息序列化成JSON 后，放到redis
 * 类似于 ArrayList,预分配空间，当长度小于1M,成倍的扩容，大于就每次多增加 1M;
 * 单个字符串 最大容量 512M，
 * 后边我们会引入位图数据机构
 */
public class StringRedis {
    public static void main(String[] args) throws InterruptedException {
        Jedis jedis = JedisUtil.getJedis();
        /**
         * 1. 简单的增删改查
        */
        // 增加，成功就返回 OK
        String res = jedis.set("name","xudong");
        System.out.println(res);
        System.out.println(jedis.get("name")); // get 方法查询
        System.out.println(jedis.exists("name")); // 查看某个键 是否存在，返回boolean 类型
        System.out.println(jedis.del("name"));// 删除，返回删除成功的个数
        /**
         * 2 批量操作
         */
        // 增加
        jedis.mset("name1","xudong1","name2","xudong2","name3","xudong3");
        List<String> mList = jedis.mget("name1","name2","name3");// 批量获取key  对应的值
        System.out.println(mList);
        /**
         * 过期和set 命令扩展
         */
        jedis.setex("name1",5,"xudong1"); // 5秒后失效
        System.out.println(jedis.get("name1"));
        Thread.sleep(6000L);
        System.out.println(jedis.get("name1")); // 输出 null ,此时失效了
        jedis.set("setnx","hello");
        System.out.println(jedis.get("setnx"));
        // setnx 加锁操作，如果存在，就不会覆盖，设置失败，常用来做为分布式锁
        jedis.setnx("setnx","hello word");
        System.out.println(jedis.get("setnx"));
        // append 操作
        jedis.append("setnx"," world");
        System.out.println(jedis.get("setnx"));

        /**
         * 计数
         * 如果 key d对应的value 整数，可以进行自增操作，范围是 long 的最大值和最小值
         */
        jedis.set("key1","28");
        jedis.incr("key1"); // 默认是自增1
        System.out.println(jedis.get("key1"));
        jedis.incrBy("key1",10L); // 自定义长度，增加10
        System.out.println(jedis.get("key1"));
    }
}
