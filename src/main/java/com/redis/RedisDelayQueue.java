package com.redis;



import com.alibaba.fastjson.TypeReference;
import redis.clients.jedis.Jedis;

import java.lang.reflect.Type;

/**
 * redis  延时队列,需要fastJson 的支持
 */
public class RedisDelayQueue <T>{
    static class TaskItem<T>{
        public String id;
        public T msg;
    }
    // fastJson 序列化对象中存在 generic 类型时，需要使用 TypeReference
    private Type TaskType = new TypeReference<TaskItem<T>>(){}.getType();

    private String key;
    private Jedis jedis;

    public RedisDelayQueue(Jedis jedis,String key) {
        this.jedis = jedis;
        this.key = key;
    }
    public void delay(){
        TaskItem<T> task = new TaskItem<T>();
    }

    public static void main(String[] args) {
        Jedis jedis= JedisUtil.getJedis();

    }
}

