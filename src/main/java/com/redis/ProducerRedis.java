package com.redis;

import redis.clients.jedis.Jedis;

/**
 * 消息队列生产者
 * 不支持消息多播，一个消费者消费完之后就没有了
 */
public class ProducerRedis {
    public static void main(String[] args) {
        Jedis jedis = JedisUtil.getJedis();
        int i=0;
        while(true){
            i++;
            jedis.lpush("mq","mq"+i);
            if(i==20){
                break;
            }
        }

    }
}
