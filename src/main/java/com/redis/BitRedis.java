package com.redis;

import com.sun.mail.util.ASCIIUtility;
import redis.clients.jedis.Jedis;

/**
 * 位图
 * 存储boolean 类型的，如果用 key?value,会很耗费内存
 * 位图 每个 只占据一个位，节省空间
 */
public class BitRedis {
    public static void main(String[] args) {
        Jedis jedis = JedisUtil.getJedis();
        /**
         * hello 的二进制编码：
         * h:01101000
         * e:01100101
         * l:01101100
         * o:01101111
         */
        String str = "hello";
        //得到hello 的编码为：0110100001100101011011000110110001101111
        String bitStr = StringToBit(str);
        jedis.del("name");
        char []chars = bitStr.toCharArray();
        System.out.println(chars);
        for (int i = 0; i < chars.length; i++) {
            // 将位置为1 的地方设置值
            if("1".equals(String.valueOf(chars[i]))){
                jedis.setbit("name",i,"1");
            }
        }
        // 零存整取，得到 hello
        System.out.println(jedis.get("name"));
        // 零取,如果是1 返回true.是0返回false
        System.out.println(jedis.getbit("name",1));
        // 获取位图上，是1 的位的个数，指定范围是1的个数,start 和end 表示的是 字符的个数，包含开头和结尾必须是8的倍数
        System.out.println(jedis.bitcount("name"));
        // 前0-3的字符， h e l l
        System.out.println(jedis.bitcount("name",0,3));
        // 2到 3的字符 l l
        System.out.println(jedis.bitcount("name",2,3));
        // 默认第一个 1 位
        System.out.println(jedis.bitpos("name",true));



    }

    /**
     * 将字符串转换成二进制
     * @param str
     * @return
     */
    public static String StringToBit(String str){
        char [] chars = str.toCharArray();
        str="";
        for (int i=0;i<chars.length;i++){
            String value = Integer.toBinaryString(chars[i]);
            str+= String.format("%08d",Integer.parseInt(value));
        }
        return str;
    }
}
