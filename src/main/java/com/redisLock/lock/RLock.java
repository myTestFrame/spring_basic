package com.redisLock.lock;

public interface RLock {
    public String lock(Long acquireTimeout,Long timeout);
    public void unLock(String value);
}
