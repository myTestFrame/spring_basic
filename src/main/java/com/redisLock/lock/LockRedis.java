package com.redisLock.lock;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.UUID;

public class LockRedis implements RLock{
    // redis 线程池
    private JedisPool jedisPool;

    public LockRedis(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }
    // 相同的key 名称，对应的value 定义为锁的 Id

    // redis 实现分布式锁。有两个超时时间问题，
    /**
     * 两个时间：
     * 1. 获取锁的超时时间，规定时间内没有得到锁，直接放弃
     * 2 ， 获取锁之后，key 有对应的有效期，会失效
     */
    private String redisLockKey = "redis_lock";

    /**
     *
     * @param acquireTimeout 获取锁的超时时间
     * @param timeout 锁失效的时间
     */
    public String lock(Long acquireTimeout,Long timeout) {
        // 1 建立redis 连接
        Jedis con = jedisPool.getResource();
        try{

            // 2 定义Key 的value ,我们生成订单号，释放锁的时候起作用
            String value = UUID.randomUUID().toString();
            // 3 定义锁失效时间,redis 认得式 秒，一般传过来的式毫秒
            int expireLock = (int) (timeout/1000);
            // 4 定义获取锁超时时间
            // 5 使用循环机制，不断的尝试获取锁
            Long endTime = System.currentTimeMillis()+acquireTimeout;
            while (System.currentTimeMillis()<endTime){
                // 获取锁,
                // 6 使用setNx 命令插入对应的redisLockKey,如果返回1 ，就得到了
                if(con.setnx(redisLockKey,value)==1){
                    con.expire(redisLockKey,expireLock);
                    return value;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con!=null)
                con.close();
        }
        // 返回为空就是一直没有获取到，放弃了
        return null;
    }


    // 释放锁
    public void unLock(String value) {
        Jedis con = jedisPool.getResource();
        try{
            if (con.get(redisLockKey).equals(value)){
                con.del(redisLockKey);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (con!=null)
                con.close();
        }
    }

}
