package com.redisLock.service;

import com.IdUtil.IdUtil;
import com.redisLock.lock.LockRedis;
import com.redisLock.lock.RLock;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class OrderService implements Runnable{
    private static JedisPool pool = null;
    static {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(200);
        config.setMaxIdle(2);
        config.setMaxWaitMillis(100*1000);
        pool = new JedisPool(config,"127.0.0.1",6379,3000);
    }
    IdUtil idUtil = new IdUtil();
    private RLock lockRedis = new LockRedis(pool);
    public void secKill(){
        String value = lockRedis.lock(30000L,30000L);
        if(value==null){
            System.out.println(Thread.currentThread().getName()+",获取锁失败");
            return;
        }
        // 获取订单号
        try {
            Thread.sleep(300);
            System.out.println(Thread.currentThread().getName()+"生产订单号"+idUtil.getOrderId());
        } catch (Exception e) {

        }
        // 释放锁
        lockRedis.unLock(value);
    }


    @Override
    public void run() {
        secKill();
    }
}
