package com.zk.lock.annotation;

import java.lang.annotation.*;

/**
 * Created by Administrator on 2019/11/8.
 */
@Target(value = {ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ZLock {
    String CONNECTION() default "127.0.0.1:2181";
    String PATH() default "/zlock";


}
