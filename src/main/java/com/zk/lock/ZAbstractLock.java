package com.zk.lock;

import com.zk.lock.ZLock;
import org.I0Itec.zkclient.ZkClient;

/**
 * Created by Administrator on 2019/11/8.
 */
public abstract class ZAbstractLock implements ZLock {
    ZkClient zkClient = null;
    String path = "";

    public ZAbstractLock(ZkClient zkClient,String path) {
        this.zkClient = zkClient;
        this.path = path;
    }

    @Override
    public void lock() {
        if(!tryLock()){
            waitLock(); // util others unlock
            lock();
        }
    }
    @Override
    public void unLock() {
        if (zkClient!=null) zkClient.close();
    }
    protected abstract void waitLock();

    protected abstract boolean tryLock();
}
