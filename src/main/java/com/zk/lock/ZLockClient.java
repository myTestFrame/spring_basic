package com.zk.lock;

import com.zk.lock.ZAbstractLock;
import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Administrator on 2019/11/8.
 */
public class ZLockClient extends ZAbstractLock {
    private  CountDownLatch countDownLatch  = null;

    public ZLockClient(ZkClient zkClient, String path) {
        super(zkClient, path);
    }

    @Override
    protected void waitLock() {
        IZkDataListener listener = new IZkDataListener() {
            @Override
            public void handleDataChange(String s, Object o) throws Exception {

            }
            @Override
            public void handleDataDeleted(String s) throws Exception {
                countDownLatch.countDown();
            }
        };
        zkClient.subscribeDataChanges(path,listener);
        if(zkClient.exists(path)){
            countDownLatch = new CountDownLatch(1);
            try {
                countDownLatch.await();// 如果不为零，会阻滞
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        zkClient.unsubscribeDataChanges(path,listener);
    }

    @Override
    protected boolean tryLock() {
        try {
            zkClient.createEphemeral(path);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
