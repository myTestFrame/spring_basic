package com.zk.lock.aop;


import com.zk.lock.ZLock;
import com.zk.lock.ZLockClient;
import org.I0Itec.zkclient.ZkClient;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;

/**
 * Created by Administrator on 2019/11/8.
 */
@Aspect
@Configuration
public class ZLockAspect {
    @Around("@annotation(com.zk.lock.annotation.ZLock)")
    public Object interceptor(ProceedingJoinPoint pjp) {
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();
        com.zk.lock.annotation.ZLock localLock = method.getAnnotation(com.zk.lock.annotation.ZLock.class);
        String zServers = localLock.CONNECTION();
        String path = localLock.PATH();
        ZLock zLock = new ZLockClient(new ZkClient(zServers),path);
        try {
            zLock.lock();
            return pjp.proceed();
        } catch (Throwable throwable) {
            throw new RuntimeException("服务器异常");
        } finally {
            zLock.unLock();
        }
    }

}
