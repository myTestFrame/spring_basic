package com.zk.service;

import com.IdUtil.IdUtil;

import com.zk.lock.ZLock;
import com.zk.lock.ZLockClient;
import org.I0Itec.zkclient.ZkClient;

/**
 * Created by Administrator on 2019/11/8.
 */

public class OrderService implements Runnable{
    IdUtil idUtil = new IdUtil();
    ZkClient zkClient = new ZkClient("127.0.0.1");
    ZLock zkeeperLock = new ZLockClient(zkClient,"/zlock");
    // 生成订单
    public  void createOrder(){
        zkeeperLock.lock();
        // 获取订单号
        String orderId = idUtil.getOrderId();
        try {
            Thread.sleep(100);
            System.out.println(Thread.currentThread().getName()+"生成订单编号："+orderId);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
          zkeeperLock.unLock();
        }
    }

    @Override
    public void run() {
        createOrder();
    }
}
