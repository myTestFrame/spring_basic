package com.thread;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Stream;

public class Demo {
    ExecutorService service = new ThreadPoolExecutor(4,10,0L, TimeUnit.SECONDS,new ArrayBlockingQueue<Runnable>(2));
    @Test
    public void test1(){
        UserInfo info = new UserInfo();
        S1 s1 =new S1(info);
        Future<UserInfo> future= service.submit(s1,info);
        System.out.println(info.getName()); // 返回空，因为 执行顺序的问题
        try {
            // get 方法是阻滞的，会等 submit 返回结果后执行下边的
            System.out.println(future.get().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void test2() throws ExecutionException, InterruptedException {
        UserInfo info = new UserInfo();
        C1 c1 =new C1(info);
        Future<UserInfo> future= service.submit(c1);
        System.out.println(future.get().getName());
    }
    @Test
    public void test3() throws InterruptedException, ExecutionException {
        List<S2> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            S2 s2 =new S2(i+"");
            list.add(s2);
        }
        // invokeAll(Collection<? extends <Callable<T>>> tasks) ,返回所有执行结果
        //invokeAny ，返回第一个返回的的,当第一个结束后，会中断其他任务
       List<Future<String>> futures=  service.invokeAll(list);
        for (Future f:futures
             ) {
            System.out.println(f.get().toString());
        }

       String future =  service.invokeAny(list);
        System.out.println(future);

    }
    @Test
    public void test4(){
        service.execute(new S1(new UserInfo()));
    }
    @Test
    public void test5() throws InterruptedException {
        ThreadPoolExecutor service1 = new ThreadPoolExecutor(5,
                10,
                1L,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(2));
        System.out.println(Thread.currentThread().getName());
        for (int i = 0; i < 2; i++) {
            service1.execute(new Runnable() {
                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName()+"执行线程");
                    try {
                        Thread.currentThread().sleep(10);
                        System.out.println(Thread.currentThread().getName()+"");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public static void main(String[] args) {
        ExecutorService service1 = new ThreadPoolExecutor(2,
                8,
                0L,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(2),
                new ThreadPoolExecutor.DiscardOldestPolicy());
            for (int i = 0; i < 15; i++) {
                service1.execute(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println(Thread.currentThread().getName() + "执行线程");
                    }
                });
            }
    }
}

class S1 implements Runnable{
    private UserInfo info;

    public S1(UserInfo info) {
        this.info = info;
    }
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
        info.setName("xudong"+info.getName());
    }
}

class UserInfo{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
class C1 implements Callable{
    private UserInfo info;

    public C1(UserInfo info) {
        this.info = info;
    }
    @Override
    public Object call() throws Exception {
        info.setName("xudong");
        return info;
    }
}
class S2 implements Callable<String>{
    private String str;

    public S2(String str) {
        this.str = str;
    }

    @Override
    public String call() throws Exception {
        System.out.println(Thread.currentThread().getName());
        return "xudong"+str;
    }
}