package com.xd.controller;

import com.xd.dao.UserMapper;
import com.xd.model.UserInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 2019/10/16.
 */
@SuppressWarnings("ALL")
@Controller
@RequestMapping("/user")
public class UserController {
    @Resource
    UserMapper userMapper;
    @RequestMapping(value = "/doRegist",method = RequestMethod.POST)
    public String doRegist(Model model, HttpServletRequest request, HttpServletResponse response){
        String loginId = request.getParameter("loginId");
        String uName = request.getParameter("uName");
        String email = request.getParameter("email");
        String password = request.getParameter("pwd");
        String tel = request.getParameter("tel");
        String sex =  request.getParameter("sex");
        String idNo = request.getParameter("idNo");

        Integer id = userMapper.getCount()+1;
        UserInfo userInfo = userMapper.selectByLoginId(loginId);
        if(userInfo!=null){
            model.addAttribute("error","用户名已存在");
            return "regist";
        }
        userInfo = new UserInfo();
        userInfo.setUserId(id.toString());
        userInfo.setLoginId(loginId);
        userInfo.setUserName(uName);
        userInfo.setPassword(password);
        userInfo.setEmail(email);
        userInfo.setIdentityCode(idNo);
        userInfo.setMobile(tel);
        userInfo.setSex(Integer.parseInt(sex));
        userMapper.insert(userInfo);
        System.out.println("注册成功..................");
        return "temp";
    }
    @RequestMapping(value = "/doLogin", method = RequestMethod.POST)
    public String doLogin(Model model, HttpServletRequest request, HttpServletResponse response) {
        String loginId = request.getParameter("uid");
        String password = request.getParameter("pwd");
        String result = "login";
        System.out.println(loginId+password);
        UserInfo userInfo = new UserInfo();
        userInfo.setLoginId(loginId);
        userInfo.setPassword(password);
        userMapper.selectAll();
        int count = userMapper.selectByPassword(userInfo);
        if (count > 0) {
            result = "index";
            model.addAttribute("userName", loginId);
            request.getSession().setAttribute("loginId",loginId);
            System.out.println("================登录验证成功！");
        } else {
            System.out.println("密码错误");
            model.addAttribute("error", "用户名或密错误");
        }
        return result;
    }
    @ResponseBody
    @RequestMapping(value = "/session",method = RequestMethod.GET)
    public String getSessionId(HttpServletRequest request){
        HttpSession session = request.getSession();
        System.out.println(" sessionid is"+session.getId());
        return "8080 端口："+session.getId();
    }
    // 退出登录状态
    @RequestMapping(value = "/out",method = RequestMethod.GET)
    public String out(HttpServletRequest request){
        HttpSession session = request.getSession();
        session.invalidate();
        return "index";
    }
}
