package com.xd.zooTest;

import org.I0Itec.zkclient.IZkChildListener;
import org.I0Itec.zkclient.ZkClient;

import java.util.ArrayList;
import java.util.List;

public class Client {
    public static List<String> listServer = new ArrayList<String>();
    public static String parent = "/test";

    public static void main(String[] args) {
        initServer();
        System.out.println(listServer);
    }
    // 获取所有的server
    public static void initServer() {
        final ZkClient zkClient = new ZkClient("127.0.0.1:2181", 60000, 1000);
        List<String> children = zkClient.getChildren(parent);
        getChilds(zkClient, children);
        // 监听父节点，节点发生变化，就会执行通知 ，我们就重新获取节点就行了
        zkClient.subscribeChildChanges(parent, new IZkChildListener() {
            public void handleChildChange(String parentPath, List<String> currentChilds) throws Exception {
                getChilds(zkClient, currentChilds);
            }
        });
    }
    private static void getChilds(ZkClient zkClient, List<String> currentChilds) {
        listServer.clear();
        for (String p : currentChilds) {
            String pathValue = (String) zkClient.readData(parent + "/" + p);
            listServer.add(pathValue);
        }
        System.out.println("从zk读取到信息:" + listServer.toString());

    }
}
