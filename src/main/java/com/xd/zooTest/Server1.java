package com.xd.zooTest;

import org.I0Itec.zkclient.ZkClient;

public class Server1{
    private static int port=8080;

    public Server1(int port) {
        this.port = port;
    }


    public static void main(String[] args) {

// 向ZooKeeper注册当前服务器
        ZkClient client = new ZkClient("127.0.0.1:2181", 60000, 1000);
        String path = "/test/server" + port;
        if (client.exists(path))
            client.delete(path);
        // path 是阶点，data 是值
        client.createEphemeral(path, "127.0.0.1:" + port);
    }
}
