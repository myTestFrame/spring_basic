package com.xd.zooTest;

import org.apache.zookeeper.*;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

public class Test001 {
    private static final String url = "127.0.0.1:2181";
    private static final int initTime = 5000;
    //信号量,阻塞程序执行,用户等待zookeeper连接成功,发送成功信号，
    private static final CountDownLatch countDownLatch = new CountDownLatch(1);

    public static void main(String[] args) {
        try {
            ZooKeeper zooKeeper = new ZooKeeper(url, initTime, new Watcher() {
                @Override
                public void process(WatchedEvent watchedEvent) {
                    // 获取事件状态
                    Event.KeeperState keeperState = watchedEvent.getState();
                    // 获取事件类型
                    Event.EventType eventType = watchedEvent.getType();
                    if (Event.KeeperState.SyncConnected == keeperState) {
                        if (Event.EventType.None == eventType) {
                            countDownLatch.countDown();
                            System.out.println("zk 启动连接...");
                        }
                        if (Event.EventType.NodeCreated == eventType){
                            System.out.println("监听到节点注册");
                        }
                    }

                }
            });
            // 进行阻塞
            countDownLatch.await();
            // 开启监听节点
            zooKeeper.exists("/temp",true);
            String node = zooKeeper.create("/temp","zhouxudong".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE,CreateMode.EPHEMERAL);
            System.out.println(node);
            ZooKeeper.States states = zooKeeper.getState();
            System.out.println(states);
            zooKeeper.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
