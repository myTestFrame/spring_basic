package com.xd.filter;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("进入过滤器");

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session =request.getSession();
        String sessionId = session.getId();
        System.out.println("sessionId is "+sessionId);
        String loginId = (String) session.getAttribute("loginId");
        System.out.println("loginId is"+loginId);
        if(loginId!=null){
            filterChain.doFilter(servletRequest,servletResponse);
        }else{
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            response.sendRedirect("/basic/html/login");
        }

    }
}
