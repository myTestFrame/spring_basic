package com.xd.lock;

import org.I0Itec.zkclient.ZkClient;

import java.util.concurrent.CountDownLatch;

public abstract class ZookeeperAbstractLock implements ExtLock {
    private static final String con = "127.0.0.1:2181";
    protected ZkClient zkClient = new ZkClient(con);
    protected String lockPath = "/lockPath";
    protected CountDownLatch countDownLatch=null;
    @Override
    public void getLock() {
        // 创建临时节点
        // 如果创建成功，执行相应的逻辑，如果失败 就等待
        // 采用时间通知，监听节点是否被删除，如果被删除，就去重新获取锁
        if(tryLock()){
            System.out.println("获取锁资源成功");
        }else {
            waitLock();
            getLock();
        }
    }
    // 如果创建失败，就等待，使用时间监听，如果节点被删除，就获取锁的资源
    abstract void waitLock();

    abstract boolean tryLock();

    // 释放锁
    @Override
    public void unLock() {
        if (zkClient!=null){
            zkClient.close();
            System.out.println("释放锁成功");
        }
    }
}
