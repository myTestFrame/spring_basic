package com.xd.lock;

public class OrderService implements Runnable{
    private OrderIdCreate orderIdCreate = new OrderIdCreate();
    private ExtLock lock= new ZookeeperLock();
    @Override
    public void run() {
        getId();
    }
    public void getId(){
        lock.getLock();
        String orderId = orderIdCreate.getId();
        System.out.println(Thread.currentThread().getName()+",订单Id:"+orderId);
        lock.unLock();
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            OrderService orderService = new OrderService();
            new Thread(orderService).start();
        }
    }
}
