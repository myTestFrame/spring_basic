package com.xd.lock;

import org.I0Itec.zkclient.IZkDataListener;

import java.util.concurrent.CountDownLatch;

public class ZookeeperLock extends ZookeeperAbstractLock {
    @Override
    void waitLock() {
        // 开启事件监听
        IZkDataListener izkDataListener = new IZkDataListener() {
            // 删除事件
            public void handleDataDeleted(String path) throws Exception {
                // 唤醒被等待的线程
                if (countDownLatch != null) {
                    countDownLatch.countDown(); // 执行止呕，才执行 await之后的代码
              }
            }
            // 节点改变事件
            public void handleDataChange(String path, Object data) throws Exception {

            }
        };
        // 注册事件
        zkClient.subscribeDataChanges(lockPath, izkDataListener);
        // 如果节点已经存在，就等待 await 的代码不会执行
        if (zkClient.exists(lockPath)){
            countDownLatch = new CountDownLatch(1);
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        // 删除监听
        zkClient.unsubscribeDataChanges(lockPath, izkDataListener);
    }

    @Override
    boolean tryLock() {
        try {
            zkClient.createEphemeral(lockPath);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
