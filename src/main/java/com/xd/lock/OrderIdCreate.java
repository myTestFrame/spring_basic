package com.xd.lock;

import java.text.SimpleDateFormat;
import java.util.Date;

// 生成订单Id
public class OrderIdCreate {
    private static int count = 0;
    public String getId(){
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        return simpleDateFormat.format(new Date())+"-"+ ++count;
    }
}
