package com.IdUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

public class IdUtil {
    public static int count=0;
    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
    public String getOrderId(){
        return simpleDateFormat.format(new Date())+"-"+ count++;
    }

}
